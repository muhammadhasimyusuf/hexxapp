<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Agency - Start Bootstrap Theme</title>
    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.15.4/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="css/styles.css" rel="stylesheet" />
    <link rel="stylesheet" href="dist/css/bootstrap.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


    <style type="text/css">
        #preload {
            z-index: 9999999999;
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background: black;
            overflow: hidden;
        }

        #preloader {
            position: relative;
            width: 70px;
            margin: 23% auto;
        }

        #preloader span {
            position: absolute;
            width: 20px;
            height: 20px;
            background: #3498db;
            opacity: 0.5;
            border-radius: 20px;
            -animation: preloader 1s infinite ease-in-out;
            -webkit-animation: preloader 1s infinite ease-in-out;
        }

        #preloader span:nth-child(2) {
            left: 20px;
            animation-delay: .2s;
            -webkit-animation-delay: .2s;
        }

        #preloader span:nth-child(3) {
            left: 40px;
            -webkit-animation-delay: .4s;
            animation-delay: .4s;
        }

        #preloader span:nth-child(4) {
            left: 60px;
            animation-delay: .6s;
            -webkit-animation-delay: .6s;
        }

        #preloader span:nth-child(5) {
            left: 80px;
            animation-delay: .8s;
            -webkit-animation-delay: .8s;
        }

        #preloader span:nth-child(6) {
            left: 100px;
            animation-delay: .10s;
            -webkit-animation-delay: .10s;
        }

        #preloader span:nth-child(7) {
            left: 120px;
            animation-delay: .12s;
            -webkit-animation-delay: .12s;
        }

        #preloader span:nth-child(8) {
            left: 140px;
            animation-delay: .16s;
            -webkit-animation-delay: .16s;
        }



        @keyframes preloader {
            0% {
                opacity: 0.3;
                transform: translateY(0px);
                box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.1);
            }

            50% {
                opacity: 1;
                transform: translateY(-10px);
                background: #f1c40f;
                box-shadow: 0px 20px 3px rgba(0, 0, 0, 0.05);
            }

            100% {
                opacity: 0.3;
                transform: translateY(0px);
                box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.1);
            }
        }

        @-webkit-keyframes preloader {
            0% {
                opacity: 0.3;
                transform: translateY(0px);
                box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.1);
            }

            50% {
                opacity: 1;
                transform: translateY(-10px);
                background: #f1c40f;
                box-shadow: 0px 20px 3px rgba(0, 0, 0, 0.05);
            }

            100% {
                opacity: 0.3;
                transform: translateY(0px);
                box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.1);
            }
        }
    </style>
</head>

<body id="page-top">
    <?php
    function error_found()
    {
        header("Location: index.html");
    }
    set_error_handler('error_found');
    ?>
    <div id="preload">
        <div id="preloader">
            <span>L</span>
            <span>O</span>
            <span>A</span>
            <span>D</span>
            <span>I</span>
            <span>N</span>
            <span>G</span>
        </div>
    </div>
    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
        <div class="container">
            <a class="navbar-brand" href="index.html"><img src="assets/img/navbar-logo.svg" alt="..." /></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                Menu
                <i class="fas fa-bars ms-1"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
            </div>
        </div>
    </nav>
    <!-- Masthead-->
    <header class="masthead">
        <?php if (isset($_POST["hexa"])) { ?>
            <div class="container">
                <div class="row">
                    <div class="col-2">

                        <p id="cnth" style="font-size: 1.5vw; text-transform: uppercase;color:red">
                            <?php
                            $hexa = isset($_POST['hexa']) ? $_POST['hexa'] : "";
                            $data = str_split($hexa);
                            ?>

                        </p>

                        <p style="font-size: 1.5vw; text-transform: uppercase; ">
                            <?php
                            foreach ($data as $dt) {
                                echo $dt;
                            }
                            ?>
                        </p>


                    </div>
                </div>
                <div class="row">





                    <P style="text-align: left; ">
                        Detail Data:
                    </P>

                    <div class="col-6" style="text-align: left;">
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingSix">
                                <button onclick="viewText('<?= $a = $data[0] . $data[1] ?>')" class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-four" aria-expanded="false" aria-controls="flush-four">
                                    <strong>CATEGORY</strong>
                                </button>
                            </h2>
                            <div id="flush-four" class="accordion-collapse collapse" aria-labelledby="flush-four">
                                <div class="accordion-body">
                                    <code>
                                        <div class="geo">
                                            <p>
                                                <?php
                                                $cat = $data[0] . $data[1];
                                                $cat1 = hexdec($cat);
                                                echo "CAT -> $cat = $cat1";
                                                ?>
                                            </p>

                                        </div>
                                    </code>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingSix">
                                <button onclick="viewText('<?= $a .= $data[2] . $data[3] . $data[4] . $data[5] ?>')" class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-three" aria-expanded="false" aria-controls="flush-three">
                                    <strong> DATA LENGTH</strong>
                                </button>
                            </h2>
                            <div id="flush-three" class="accordion-collapse collapse" aria-labelledby="flush-three">
                                <div class="accordion-body">
                                    <code>
                                        <div class="geo">

                                            <P>
                                                <?php

                                                $len = strtoupper($data[2] . $data[3] . $data[4] . $data[5]);
                                                $len1 = hexdec($len);
                                                echo "LEN -> $len = $len1";
                                                ?>
                                            </P>
                                        </div>
                                    </code>
                                </div>
                            </div>
                        </div>

                        <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingSix">
                                <button onclick="viewText('<?= $a .= $data[6] . $data[7] . $data[8] . $data[9] . $data[10] . $data[11] . $data[12] . $data[13] ?>')" class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-two" aria-expanded="false" aria-controls="flush-two">
                                    <strong>FSPEC</strong>
                                </button>
                            </h2>
                            <div id="flush-two" class="accordion-collapse collapse" aria-labelledby="flush-two">
                                <div class="accordion-body">
                                    <code>
                                        <div class="geo">
                                            <p>
                                                <?php
                                                $spec = strtoupper($data[6] . $data[7] . $data[8] . $data[9] . $data[10] . $data[11] . $data[12] . $data[13]);

                                                echo "FSPEC        -> $spec";

                                                ?>
                                            </p>
                                            <p>
                                                <?php
                                                $spec1 = decbin(hexdec($spec));
                                                echo "FSPEC BINNER ->$spec1";
                                                ?>
                                            </p>
                                        </div>
                                    </code>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingFour">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-one" aria-expanded="false" aria-controls="flush-one">
                                    <strong>ASTERIX MESSAGE</strong>
                                </button>
                            </h2>
                            <div id="flush-one" class="accordion-collapse collapse collapse show" aria-labelledby="flush-one">
                                <div class="accordion-body">
                                    <code>
                                        <div class="accordion accordion-flush" id="accordionFlushExample">
                                            <div class="accordion-item">
                                                <h2 class="accordion-header" id="flush-headingOne">
                                                    <button onclick="viewText('<?= $a .= $data[14] . $data[15] . $data[16] . $data[17] ?>')" class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                                                        Data Source Identification
                                                    </button>
                                                </h2>
                                                <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne">
                                                    <div class="accordion-body">
                                                        <code>

                                                            <?php

                                                            $kota = [
                                                                '136' => 'Pontianak',
                                                                '141' => 'Pagkalan Bun',
                                                                '142' => 'Palu',
                                                                '143' => 'Sorong',
                                                                '144' => 'Makassar',
                                                                '145' => 'Waingapu',
                                                                '146' => 'Saumlaki',
                                                                '147' => 'Kintamani',
                                                                '148' => 'Tarakan',
                                                                '149' => 'Galela',
                                                                '150' => 'Alor',
                                                                '151' => 'Ambon',
                                                                '152' => 'Kendari',
                                                                '153' => 'Timika',
                                                                '154' => 'Merauke',
                                                                '155' => 'Kupang'
                                                            ];
                                                            ?>

                                                            <div class="box">
                                                                <div class="DSI" style="margin-bottom: 10px;">
                                                                    <P>SAC : <?php echo hexdec($data[14] . $data[15]) ?></P>
                                                                    <P>SIC : <?php echo hexdec($data[16] . $data[17]) ?></P>
                                                                    <p>Kota : <?php $nkota = hexdec($data[16] . $data[17]);
                                                                                echo $kota[$nkota]
                                                                                ?> </p>
                                                                </div>
                                                        </code>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="accordion-item">
                                                <h2 class="accordion-header" id="flush-headingTwo">
                                                    <button onclick="viewText('<?= $a .= $data[18] . $data[19] . $data[20] . $data[21] ?>')" class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                                                        Target Report Descriptor
                                                    </button>
                                                </h2>
                                                <div id="flush-collapseTwo" class="accordion-collapse collapse" aria-labelledby="flush-headingTwo">
                                                    <div class="accordion-body">
                                                        <code>
                                                            <div class="TRD" style="margin-bottom: 10px;">

                                                                <p> <?php
                                                                    $bin = sprintf("%08d", decbin(hexdec($data[18] . $data[19])));
                                                                    $bin1 = sprintf("%08d", decbin(hexdec($data[20] . $data[21])));
                                                                    $bin2 = decbin(hexdec($data[18] . $data[19]));
                                                                    ?> </p>
                                                                <p>
                                                                    <?php
                                                                    $binner = $bin . $bin1;
                                                                    echo $binner;
                                                                    ?>
                                                                </p>

                                                                <?php
                                                                $strbin = str_split($binner);
                                                                ?>
                                                                <p style="text-transform: uppercase;">
                                                                    <?php if ($strbin[0] == "1") {
                                                                        echo "dcr : 1 (Differential correction)";
                                                                    } else {
                                                                        echo "dcr : 0 (No Differential correction)";
                                                                    }   ?> <br>
                                                                    <?php if ($strbin[1] != "0") {
                                                                        echo "gbs : 1 (Ground bit set)";
                                                                    } else {
                                                                        echo "gbs : 0 (Ground bit not set)";
                                                                    }   ?> <br>
                                                                    <?php if ($strbin[2] != "0") {
                                                                        echo "sim : 1 (simulated target report)";
                                                                    } else {
                                                                        echo "sim : 0 (actual target report)";
                                                                    }   ?> <br>
                                                                    <?php if ($strbin[3] != "0") {
                                                                        echo "TST : 1 (test target)";
                                                                    } else {
                                                                        echo "TST : 0 (default)";
                                                                    }   ?> <br>
                                                                    <?php if ($strbin[4] != "0") {
                                                                        echo "rab : 1 (report from field monitor)";
                                                                    } else {
                                                                        echo "rab : 0 (report from target transponder)";
                                                                    }   ?> <br>
                                                                    <?php if ($strbin[5] != "0") {
                                                                        echo "saa : 1 ( equipment capable to provide selected altitude)";
                                                                    } else {
                                                                        echo "saa : 0 ( equipment not capable to provide selected altitude)";
                                                                    }   ?> <br>
                                                                    <?php if ($strbin[6] != "0") {
                                                                        echo "spi : 1 (special position identification)";
                                                                    } else {
                                                                        echo "spi : 0 (absence of SPI)";
                                                                    }   ?> <br>

                                                                    <?php $atp = decbin($strbin[8] . $strbin[9] . $strbin[10]);
                                                                    if ($atp == 1) {
                                                                        echo "atp : 1 (24-bit ICAO adrress)";
                                                                    } elseif ($atp == 2) {
                                                                        echo "atp : 2 Surface vechile address";
                                                                    } elseif ($atp == 3) {
                                                                        echo "atp : 3 Anonymous address";
                                                                    } elseif ($atp == 4 || 5 || 6 || 7) {
                                                                        echo "Reserved for future use";
                                                                    } else {
                                                                        echo "Non unique address";
                                                                    }
                                                                    ?><br>

                                                                    <?php $arc = decbin($strbin[11] . $strbin[12]);
                                                                    if ($arc == 1) {
                                                                        echo "altitude : 25 ft";
                                                                    } elseif ($arc == 2) {
                                                                        echo "altitude : 25 ft";
                                                                    } else {
                                                                        echo "unknwon";
                                                                    }
                                                                    ?>
                                                                </p>
                                                            </div>
                                                    </div>
                                    </code>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingThree">
                                <button onclick="viewText('<?= $a .= $data[22] . $data[23] . $data[24] . $data[25] . $data[26] . $data[27] ?>')" class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false" aria-controls="flush-collapseThree">
                                    Time Of Day
                                </button>
                            </h2>
                            <div id="flush-collapseThree" class="accordion-collapse collapse" aria-labelledby="flush-headingThree">
                                <div class="accordion-body">
                                    <code>
                                        <div class=" TOD">
                                            <?php
                                            $toddata = $data[22] . $data[23];
                                            $toddata1 = $data[24] . $data[25];
                                            $toddata2 = $data[26] . $data[27];

                                            echo strtoupper($toddata . $toddata1 . $toddata2);
                                            ?>
                                            <p style="text-transform: uppercase;">
                                                TOD ->
                                                <?php $tod = hexdec($toddata . $toddata1 . $toddata2);
                                                $tod1 = $tod / 128;
                                                $tod2 = $tod1 / 3600;
                                                $tod2explode = explode(".", $tod2);
                                                $tod2explode1 = 0 . "." . $tod2explode[1];
                                                $jam = $tod2explode[0];
                                                $tod3 = $tod2explode1 * 60;
                                                $tod3explode =  explode(".", $tod3);
                                                $menit = $tod3explode[0];
                                                $tod3explode1 = 0 . "." . $tod3explode[1];
                                                $detik = $tod3explode1 * 60;
                                                echo $jam;
                                                echo ":";
                                                echo $menit;
                                                echo ":";
                                                echo round($detik, 3);
                                                ?>
                                            </p>
                                        </div>
                                    </code>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingFour">
                                <button onclick="viewText('<?= $a .= $data[28] . $data[29] . $data[30] . $data[31] . $data[32] . $data[33] . $data[34] . $data[35] . $data[36] . $data[37] . $data[38] . $data[39] ?>')" class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFour" aria-expanded="false" aria-controls="flush-collapseFour">
                                    Position in WGS-84 Co-ordinates
                                </button>
                            </h2>
                            <div id="flush-collapseFour" class="accordion-collapse collapse" aria-labelledby="flush-headingFour">
                                <div class="accordion-body"><code>
                                        <div class="position">
                                            <p style="text-transform: uppercase;">
                                                <?php
                                                $position = hexdec($data[28] . $data[29] . $data[30] . $data[31] . $data[32] . $data[33]);
                                                $latitude = $position * 180 / pow(2, 23);
                                                echo "latitude : ";
                                                echo 360 - $latitude;
                                                echo "<br>";
                                                $position1 = hexdec($data[34] . $data[35] . $data[36] . $data[37] . $data[38] . $data[39]);
                                                $longitude = $position1 * 180 / pow(2, 23);
                                                echo "longitude : $longitude";
                                                ?>
                                            </p>
                                        </div>
                                    </code></div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingFive">
                                <button onclick="viewText('<?= $a .= $data[40] . $data[41] . $data[42] . $data[43] . $data[44] . $data[45] ?>')" class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseFive" aria-expanded="false" aria-controls="flush-collapseFive">
                                    Target Address
                                </button>
                            </h2>
                            <div id="flush-collapseFive" class="accordion-collapse collapse" aria-labelledby="flush-headingFive">
                                <div class="accordion-body">
                                    <code>
                                        <div class="target">
                                            <p style="text-transform: uppercase;">
                                                <?php
                                                $tadd = hexdec($data[40] . $data[41] . $data[42] . $data[43] . $data[44] . $data[45]);
                                                echo "TADD : $tadd"
                                                ?>
                                            </p>
                                        </div>
                                    </code>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingSix">
                                <button onclick="viewText('<?= $a .= $data[46] . $data[47] . $data[48] . $data[49] ?>')" class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseSix" aria-expanded="false" aria-controls="flush-collapseSix">
                                    Geometric Altitude
                                </button>
                            </h2>
                            <div id="flush-collapseSix" class="accordion-collapse collapse" aria-labelledby="flush-headingSix">
                                <div class="accordion-body">
                                    <code>
                                        <div class="geo">
                                            <p>
                                                <?php
                                                $geo = hexdec($data[46] . $data[47] . $data[48] . $data[49]);
                                                $geo1 = $geo * 6.25;
                                                echo "GALT : $geo1 ft"
                                                ?>
                                            </p>
                                        </div>
                                    </code>
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingSeven">
                                <button onclick="viewText('<?= $a .= $data[50] . $data[51] . $data[52] . $data[53] ?>')" class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#flush-collapseSeven" aria-expanded="false" aria-controls="flush-collapseSeven">
                                    Figure Of Merit
                                </button>
                            </h2>
                            <div id="flush-collapseSeven" class="accordion-collapse collapse" aria-labelledby="flush-headingSeven">
                                <div class="accordion-body">
                                    <code>
                                        <div class="fom">
                                            <p>
                                                <?php
                                                $bin2 = sprintf("%08d", decbin(hexdec($data[50] . $data[51])));
                                                $bin3 = sprintf("%08d", decbin(hexdec($data[52] . $data[53])));
                                                $fom = $bin2 . $bin3;
                                                echo $fom;
                                                echo "<br>";
                                                $strfom = str_split($fom);
                                                $pa = bindec($strfom[12] . $strfom[13] . $strfom[14] . $strfom[15]);
                                                echo "PA : $pa";
                                                echo "<br>";
                                                $dc = bindec($strfom[4] . $strfom[5]);
                                                echo "DC : $dc";
                                                echo "<br>";
                                                $mn = bindec($strfom[2] . $strfom[3]);
                                                echo "MN : $mn";
                                                echo "<br>";
                                                $ac = bindec($strfom[0] . $strfom[1]);
                                                echo "AC : $ac";
                                                ?>
                                            </p>

                                        </div>
                                    </code>
                                </div>
                            </div>
                        </div>
                        </code>
                    </div>
                </div>
            </div>
            </div>
            </div>
            <div class="col-6">
                <img src="assets/img/Table1.jpg" alt="" class="img-fluid">
            </div>
            </div>

        <?php  }; ?>







        </div>

        </div>
    </header>

    <!-- Footer-->
    <footer class="footer py-4">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-4 text-lg-start">Copyright &copy; Your Website 2021</div>
                <div class="col-lg-4 my-3 my-lg-0">
                    <a class="btn btn-dark btn-social mx-2" href="#!"><i class="fab fa-twitter"></i></a>
                    <a class="btn btn-dark btn-social mx-2" href="#!"><i class="fab fa-facebook-f"></i></a>
                    <a class="btn btn-dark btn-social mx-2" href="#!"><i class="fab fa-linkedin-in"></i></a>
                </div>
                <div class="col-lg-4 text-lg-end">
                    <a class="link-dark text-decoration-none me-3" href="#!">Privacy Policy</a>
                    <a class="link-dark text-decoration-none" href="#!">Terms of Use</a>
                </div>
            </div>
        </div>
    </footer>
    <!-- Portfolio Modals-->
    <!-- Portfolio item 1 modal popup-->
    <div class="portfolio-modal modal fade" id="portfolioModal1" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-bs-dismiss="modal"><img src="assets/img/close-icon.svg" alt="Close modal" /></div>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8">
                            <div class="modal-body">
                                <!-- Project details-->
                                <h2 class="text-uppercase">Project Name</h2>
                                <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                                <img class="img-fluid d-block mx-auto" src="assets/img/portfolio/1.jpg" alt="..." />
                                <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                                <ul class="list-inline">
                                    <li>
                                        <strong>Client:</strong>
                                        Threads
                                    </li>
                                    <li>
                                        <strong>Category:</strong>
                                        Illustration
                                    </li>
                                </ul>
                                <button class="btn btn-primary btn-xl text-uppercase" data-bs-dismiss="modal" type="button">
                                    <i class="fas fa-times me-1"></i>
                                    Close Project
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Portfolio item 2 modal popup-->
    <div class="portfolio-modal modal fade" id="portfolioModal2" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-bs-dismiss="modal"><img src="assets/img/close-icon.svg" alt="Close modal" /></div>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8">
                            <div class="modal-body">
                                <!-- Project details-->
                                <h2 class="text-uppercase">Project Name</h2>
                                <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                                <img class="img-fluid d-block mx-auto" src="assets/img/portfolio/2.jpg" alt="..." />
                                <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                                <ul class="list-inline">
                                    <li>
                                        <strong>Client:</strong>
                                        Explore
                                    </li>
                                    <li>
                                        <strong>Category:</strong>
                                        Graphic Design
                                    </li>
                                </ul>
                                <button class="btn btn-primary btn-xl text-uppercase" data-bs-dismiss="modal" type="button">
                                    <i class="fas fa-times me-1"></i>
                                    Close Project
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Portfolio item 3 modal popup-->
    <div class="portfolio-modal modal fade" id="portfolioModal3" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-bs-dismiss="modal"><img src="assets/img/close-icon.svg" alt="Close modal" /></div>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8">
                            <div class="modal-body">
                                <!-- Project details-->
                                <h2 class="text-uppercase">Project Name</h2>
                                <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                                <img class="img-fluid d-block mx-auto" src="assets/img/portfolio/3.jpg" alt="..." />
                                <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                                <ul class="list-inline">
                                    <li>
                                        <strong>Client:</strong>
                                        Finish
                                    </li>
                                    <li>
                                        <strong>Category:</strong>
                                        Identity
                                    </li>
                                </ul>
                                <button class="btn btn-primary btn-xl text-uppercase" data-bs-dismiss="modal" type="button">
                                    <i class="fas fa-times me-1"></i>
                                    Close Project
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Portfolio item 4 modal popup-->
    <div class="portfolio-modal modal fade" id="portfolioModal4" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-bs-dismiss="modal"><img src="assets/img/close-icon.svg" alt="Close modal" /></div>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8">
                            <div class="modal-body">
                                <!-- Project details-->
                                <h2 class="text-uppercase">Project Name</h2>
                                <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                                <img class="img-fluid d-block mx-auto" src="assets/img/portfolio/4.jpg" alt="..." />
                                <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                                <ul class="list-inline">
                                    <li>
                                        <strong>Client:</strong>
                                        Lines
                                    </li>
                                    <li>
                                        <strong>Category:</strong>
                                        Branding
                                    </li>
                                </ul>
                                <button class="btn btn-primary btn-xl text-uppercase" data-bs-dismiss="modal" type="button">
                                    <i class="fas fa-times me-1"></i>
                                    Close Project
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Portfolio item 5 modal popup-->
    <div class="portfolio-modal modal fade" id="portfolioModal5" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-bs-dismiss="modal"><img src="assets/img/close-icon.svg" alt="Close modal" /></div>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8">
                            <div class="modal-body">
                                <!-- Project details-->
                                <h2 class="text-uppercase">Project Name</h2>
                                <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                                <img class="img-fluid d-block mx-auto" src="assets/img/portfolio/5.jpg" alt="..." />
                                <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                                <ul class="list-inline">
                                    <li>
                                        <strong>Client:</strong>
                                        Southwest
                                    </li>
                                    <li>
                                        <strong>Category:</strong>
                                        Website Design
                                    </li>
                                </ul>
                                <button class="btn btn-primary btn-xl text-uppercase" data-bs-dismiss="modal" type="button">
                                    <i class="fas fa-times me-1"></i>
                                    Close Project
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Portfolio item 6 modal popup-->
    <div class="portfolio-modal modal fade" id="portfolioModal6" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="close-modal" data-bs-dismiss="modal"><img src="assets/img/close-icon.svg" alt="Close modal" /></div>
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-lg-8">
                            <div class="modal-body">
                                <!-- Project details-->
                                <h2 class="text-uppercase">Project Name</h2>
                                <p class="item-intro text-muted">Lorem ipsum dolor sit amet consectetur.</p>
                                <img class="img-fluid d-block mx-auto" src="assets/img/portfolio/6.jpg" alt="..." />
                                <p>Use this area to describe your project. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Est blanditiis dolorem culpa incidunt minus dignissimos deserunt repellat aperiam quasi sunt officia expedita beatae cupiditate, maiores repudiandae, nostrum, reiciendis facere nemo!</p>
                                <ul class="list-inline">
                                    <li>
                                        <strong>Client:</strong>
                                        Window
                                    </li>
                                    <li>
                                        <strong>Category:</strong>
                                        Photography
                                    </li>
                                </ul>
                                <button class="btn btn-primary btn-xl text-uppercase" data-bs-dismiss="modal" type="button">
                                    <i class="fas fa-times me-1"></i>
                                    Close Project
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Bootstrap core JS-->
    <script src="js/js.js"></script>
    <!-- Core theme JS-->
    <script src="js/scripts.js"></script>
    <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
    <!-- * *                               SB Forms JS                               * *-->
    <!-- * * Activate your form at https://startbootstrap.com/solution/contact-forms * *-->
    <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
    <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>

    <script>
        function viewText(name) {

            document.getElementById("cnth").innerHTML = name;
        }
    </script>
    <script>
        var loader = document.getElementById('preload');


        //Hide the spinner after 2 seconds
        setTimeout(
            function() {
                var id200 = document.getElementById("preload");
                id200.style.transition = "opacity " + 1 + "s";
                id200.style.opacity = 0;
                id200.addEventListener("transitionend", function() {
                    console.log("transition has ended, set display: none;");
                    id200.style.display = "none";
                });
            }, 2000
        );
    </script>
</body>

</html>