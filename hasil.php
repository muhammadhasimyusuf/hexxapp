<?php
session_start(); 
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>APLIKASI ASTERIX</title>
    <!-- Favicon-->
    <link rel="icon" type="image/x-icon" href="assets/favicon.ico" />
    <!-- Font Awesome icons (free version)-->
    <script src="https://use.fontawesome.com/releases/v5.15.4/js/all.js" crossorigin="anonymous"></script>
    <!-- Google fonts-->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css" />
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700" rel="stylesheet" type="text/css" />
    <!-- Core theme CSS (includes Bootstrap)-->
    <link href="css/styles.css" rel="stylesheet" />
    <link rel="stylesheet" href="dist/css/bootstrap.min.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


    <style type="text/css">
        #preload {
            z-index: 9999999999;
            position: fixed;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background: black;
            overflow: hidden;
        }

        #preloader {
            position: relative;
            width: 70px;
            margin: 23% auto;
        }

        #preloader span {
            position: absolute;
            width: 20px;
            height: 20px;
            background: #3498db;
            opacity: 0.5;
            border-radius: 20px;
            -animation: preloader 1s infinite ease-in-out;
            -webkit-animation: preloader 1s infinite ease-in-out;
        }

        #preloader span:nth-child(2) {
            left: 20px;
            animation-delay: .2s;
            -webkit-animation-delay: .2s;
        }

        #preloader span:nth-child(3) {
            left: 40px;
            -webkit-animation-delay: .4s;
            animation-delay: .4s;
        }

        #preloader span:nth-child(4) {
            left: 60px;
            animation-delay: .6s;
            -webkit-animation-delay: .6s;
        }

        #preloader span:nth-child(5) {
            left: 80px;
            animation-delay: .8s;
            -webkit-animation-delay: .8s;
        }

        #preloader span:nth-child(6) {
            left: 100px;
            animation-delay: .10s;
            -webkit-animation-delay: .10s;
        }

        #preloader span:nth-child(7) {
            left: 120px;
            animation-delay: .12s;
            -webkit-animation-delay: .12s;
        }

        #preloader span:nth-child(8) {
            left: 140px;
            animation-delay: .16s;
            -webkit-animation-delay: .16s;
        }



        @keyframes preloader {
            0% {
                opacity: 0.3;
                transform: translateY(0px);
                box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.1);
            }

            50% {
                opacity: 1;
                transform: translateY(-10px);
                background: #f1c40f;
                box-shadow: 0px 20px 3px rgba(0, 0, 0, 0.05);
            }

            100% {
                opacity: 0.3;
                transform: translateY(0px);
                box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.1);
            }
        }

        @-webkit-keyframes preloader {
            0% {
                opacity: 0.3;
                transform: translateY(0px);
                box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.1);
            }

            50% {
                opacity: 1;
                transform: translateY(-10px);
                background: #f1c40f;
                box-shadow: 0px 20px 3px rgba(0, 0, 0, 0.05);
            }

            100% {
                opacity: 0.3;
                transform: translateY(0px);
                box-shadow: 0px 0px 3px rgba(0, 0, 0, 0.1);
            }
        }
    </style>
</head>

<body id="page-top">

    <div id="preload">
        <div id="preloader">
            <span>L</span>
            <span>O</span>
            <span>A</span>
            <span>D</span>
            <span>I</span>
            <span>N</span>
            <span>G</span>
        </div>
    </div>
    <!-- Navigation-->
    <nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
        <div class="container">
            <a class="navbar-brand" href="index.html"><img src="assets/img/navbar-logo.png
                " alt="..." /></a>
            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarResponsive"
                aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                Menu
                <i class="fas fa-bars ms-1"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
                <ul class="navbar-nav text-uppercase ms-auto py-4 py-lg-0">
                    <li class="nav-item">
                        <a class="nav-link" href="print.php">Print</a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>
    <!-- Masthead-->
    <header class="masthead">

        <div class="container" style="margin-bottom:500px">


            <div class="row">
                <div>
                    <p style="font-size: 17px; text-align: left; text-transform: uppercase; margin-top:-150px">
                        <?php
                        
                    $hexa = isset($_POST['hexa']) ? $_POST['hexa'] : "";
                    if(preg_match('/\s/', $hexa)){
                        echo $hexa;
                    }else{
                        $show = str_split($hexa, 2);
                        $showhex = '';
                        foreach($show as $s){
                            $showhex .= " ".$s;
                        }
                        echo $showhex;

                    }
                    $_SESSION['hexa'] = $hexa;

                    ?>
                    </p>
                </div>
            </div>
            <p style="text-align: left; margin-top:-100px">
                Detail Data:
            </p>
            <div style="float:right ;" class="col-5">
            <?php if(substr($hexa,0, 2) == 30) { ;?>
                <h3>
                    Tabel Standar UAP Category 48
                </h3>
                <img src="assets/img/48.jpeg" alt="" class="img-fluid">
            <?php }else{ ;?>
            <span>
                <h3>
                    Tabel Standar UAP Category 34
                </h3>
            </span>
            <img src="assets/img/Table1.jpg" alt="" class="img-fluid">
            <?php }; ?>

            </div>
            <div class="col-6" style="text-align: left;">
                <?php if (isset($_POST["hexa"])) { ?>
                <?php if(preg_match('/\s/', $hexa)){
                    $data = explode(" ", $hexa);
                }else{
                    $data = str_split($hexa, 2);
                } 
                 ?>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="flush-headingSixx">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#flush-twoo" aria-expanded="false" aria-controls="flush-two">
                            <strong>CATEGORY</strong>

                        </button>
                    </h2>
                    <div id="flush-twoo" class="accordion-collapse collapse" aria-labelledby="flush-twoo">
                        <div class="accordion-body">
                            <code>
                                <div class="geo">
                                    <p>
                                        <?php
                                                                $cat = hexdec($data[0]);
                                                                echo "CAT : $data[0] = $cat";
                                                                $_SESSION['cat'] = $cat;
                                                                $_SESSION['cat']
                                                                ?>
                                    </p>

                                </div>
                            </code>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="flush-headingSix">
                        <button onclick="viewText('<?= $a .= $data[2] . $data[3] . $data[4] . $data[5] ?>')"
                            class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#flush-three" aria-expanded="false" aria-controls="flush-three">
                            <strong> DATA LENGTH</strong>
                        </button>
                    </h2>
                    <div id="flush-three" class="accordion-collapse collapse" aria-labelledby="flush-three">
                        <div class="accordion-body">
                            <code>
                                <div class="geo">

                                    <p>
                                        <?php
                                                                $len = hexdec($data[1] . $data[2]);
                                                                $len1 = $data[1] . $data[2];
                                                                echo "LEN : $len1 = $len";
                                                                $_SESSION['len'] = $len;
                                                                $_SESSION['len1'] = $len1;
                                                                ?>
                                    </p>
                                </div>
                            </code>
                        </div>
                    </div>
                </div>
                <?php if ($cat == 34){;  ?>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="flush-headingSix">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#flush-two" aria-expanded="false" aria-controls="flush-two">
                            <strong>FSPEC</strong>

                        </button>
                    </h2>
                    <div id="flush-two" class="accordion-collapse collapse" aria-labelledby="flush-two">
                        <div class="accordion-body">
                            <code>
                                <div class="geo">
                                    <p>
                                        <?php
                                               $spec = strtoupper($data[3]);
                                                echo "FSPEC        : $spec";
                                                $_SESSION['spec'] = $spec;

                                                ?>
                                    </p>
                                    <p>
                                        <?php
                                                $spec1 = decbin(hexdec($spec));
                                                echo "FSPEC BINNER :$spec1";
                                                $_SESSION['spec1'] = $spec1;
                                                ?>
                                    </p>
                                </div>
                            </code>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="flush-headingFour">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#flush-one" aria-expanded="false" aria-controls="flush-one">
                            <strong>ASTERIX Message</strong>

                        </button>
                    </h2>
                    <div id="flush-one" class="accordion-collapse collapse collapse show" aria-labelledby="flush-one">
                        <div class="accordion-body">
                            <code>
                                <div class="accordion accordion-flush" id="accordionFlushExample">
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="flush-headingOne">
                                            <button style="color:gray" class="accordion-button collapsed" type="button"
                                                data-bs-toggle="collapse" data-bs-target="#flush-collapseOne"
                                                aria-expanded="false" aria-controls="flush-collapseOne">
                                                <b> Data Source Identification</b>
                                            </button>
                                        </h2>
                                        <div id="flush-collapseOne" class="accordion-collapse collapse show"
                                            aria-labelledby="flush-headingOne">
                                            <div class="accordion-body">
                                                <code>
                                                    <?php

                                                        $kota = [
                                                              '1' => 'Medan', 
                                                              '2' => 'Pekanbaru', 
                                                              '3' => 'Tanjung Pinang', 
                                                              '4' => 'Palembang', 
                                                              '5' => 'Jakarta', 
                                                              '6' => 'Pontianak', 
                                                              '7' => 'Yogyakarta', 
                                                              '8' => 'Surabaya', 
                                                              '9' => 'Makassar', 
                                                              '10' => 'Banjarmasin', 
                                                              '11' => 'Balikpapan', 
                                                              '12' => 'Denpasar', 
                                                              '13' => 'Biak', 

                                                            
                                                        ]
                                                        ?>

                                                    <div class="box">
                                                        <div class="DSI" style="margin-bottom: 10px;">
                                                            SAC : <?php echo ($data[4]) ?> ->
                                                            <?php echo hexdec($data[4]) ?></p>
                                                            <p>SIC : <?php echo ($data[5]) ?> ->
                                                                <?php echo hexdec($data[5]) ?></p>
                                                            <p>Kota : 
                                                                <?php echo hexdec($data[5]) ? $kota[hexdec($data[5])] : ''; ?></p>
                                                            <!-- <p>Kota :
                                                                    <?php $nkota = hexdec($data[5]);
                                                                            echo $nkota ? $kota[$nkota] : '';
                                                                            $city = $nkota ? $kota[$nkota] : '';
                                                                            $sac= hexdec($data[4]);
                                                                            $sic= hexdec($data[5]);
                                                                            $_SESSION['sic'] = $sic;
                                                                            $_SESSION['sac'] = $sac;
                                                                            $_SESSION['kota'] = $city;
                                                                            ?>
                                                                </p> -->
                                                        </div>
                                                </code>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="flush-headingTwo">
                                            <button style="color:gray" class="accordion-button collapsed" type="button"
                                                data-bs-toggle="collapse" data-bs-target="#flush-collapseTwo"
                                                aria-expanded="false" aria-controls="flush-collapseTwo">
                                                <b> Message Type </b>
                                            </button>
                                        </h2>
                                        <div id="flush-collapseTwo" class="accordion-collapse collapse"
                                            aria-labelledby="flush-headingTwo">
                                            <div class="accordion-body">
                                                <code>
                                                    <div class="MT" style="margin-bottom: 10px;">

                                                        <p><?php
                                                                    $dec = sprintf("%03d", hexdec($data[6]));
                                                                    ?></p>
                                                        <p>
                                                            <?php
                                                                    if($dec == 001){
                                                                    echo "$dec : North Marker Message";
                                                                    $_SESSION['decData'] = 'North Marker Message';
                                                                    }
                                                                    if($dec == 002){
                                                                    echo "$dec : Sector Crossing Message";
                                                                    
                                                                    $_SESSION['decData'] = 'Sector Crossing Message';
                                                                    }
                                                                    if($dec == 003){
                                                                    echo "$dec : Geographical Filtering Messahe";
                                                                    
                                                                    $_SESSION['decData'] = 'Geographical Filtering Messahe';
                                                                    }
                                                                    if($dec == 004){
                                                                    echo "$dec : Jamming Strobe Message";
                                                                    
                                                                    $_SESSION['decData'] = 'Jamming Strobe Message';
                                                                    }
                                                                    ?>
                                                        </p>


                                                        <p style="text-transform: uppercase;">
                                                            <?php 
                                                            $_SESSION['dec'] = $dec;
                                                                ?>

                                                        </p>
                                                    </div>
                                            </div>
                            </code>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="flush-headingThree">
                        <button style="color:gray" class="accordion-button collapsed" type="button"
                            data-bs-toggle="collapse" data-bs-target="#flush-collapseThree" aria-expanded="false"
                            aria-controls="flush-collapseThree">
                            <b>Time Of Day</b>

                        </button>
                    </h2>
                    <div id="flush-collapseThree" class="accordion-collapse collapse"
                        aria-labelledby="flush-headingThree">
                        <div class="accordion-body">
                            <code>
                                <div class=" TOD">

                                    <?php echo $data[7] ?>
                                    <?php echo $data[8] ?>
                                    <?php echo $data[9] ?>

                                    <p style="text-transform: uppercase;">
                                        TOD ->
                                        <?php $tod = hexdec($data[7] . $data[8] . $data[9]);
                                                                    $tod1 = $tod / 128;
                                                                    $tod2 = $tod1 / 3600;
                                                                    $tod2explode = explode(".", $tod2);
                                                                    $tod2explode1 = 0 . "." . $tod2explode[1];
                                                                    $jam = $tod2explode[0];
                                                                    $tod3 = $tod2explode1 * 60;
                                                                    $tod3explode =  explode(".", $tod3);
                                                                    $menit = $tod3explode[0];
                                                                    $tod3explode1 = 0 . "." . $tod3explode[1];
                                                                    $detik = $tod3explode1 * 60;
                                                                    echo $jam;
                                                                    echo ":";
                                                                    echo $menit;
                                                                    echo ":";
                                                                    echo round($detik, 3);
                                                                    $waktu = $jam.":".$menit.":".round($detik, 3);
                                                                    $_SESSION['tod'] = $tod;
                                                                    $_SESSION['waktu'] = $waktu;
                                                                    ?>
                                    </p>
                                </div>
                            </code>
                        </div>
                    </div>
                </div>
                  <?php if($dec == 002) { ;?>                                                    
                <div class="accordion-item">
                    <h2 class="accordion-header" id="flush-headingFour">
                        <button style="color:gray" class="accordion-button collapsed" type="button"
                            data-bs-toggle="collapse" data-bs-target="#flush-collapseFour" aria-expanded="false"
                            aria-controls="flush-collapseFour">
                            <b>Sector Number</b>

                        </button>
                    </h2>
                    <div id="flush-collapseFour" class="accordion-collapse collapse"
                        aria-labelledby="flush-headingFour">
                        <div class="accordion-body">
                            <code>
                                <div class="position">
                                    <p style="text-transform: uppercase;">
                                        <?php
                                            $Sector = $data[10];
                                            $Seconv = hexdec($Sector);
                                            $secresult = $Seconv * 1.40625;
                                            echo "SN : $Sector -> $Seconv";
                                            echo "<br>";
                                            echo "SN : $Seconv x 1.4 = $secresult&deg;";
                                            $_SESSION['Sector'] = $Sector;
                                            $_SESSION['secresult'] = $secresult;
                                        ?>
                                    </p>
                                </div>
                            </code>
                        </div>
                    </div>
                </div>
                    <?php };?>

        <?php  }elseif($cat = 48){; ?>
            <?php $fspec = '' ?>
            <?php  foreach($data as $key=>$d){
                    if($key > 2){
                        $bindata = str_split(decbin(hexdec($d)));
                        $bindata = end($bindata);
                        $fspec .= $d;
                        if($bindata == 0){
                            $keydata = $key;
                            // var_dump($key);
                            break;
                        }
                     }
                }  ?>
            <?php  foreach($data as $key=>$d){
                    if($key > $keydata){
                        $keystart[] = $key;
                     }
                }  ?>
            <div class="accordion-item">
                    <h2 class="accordion-header" id="flush-headingSix">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#flush-two" aria-expanded="false" aria-controls="flush-two">
                            <strong>FSPEC</strong>

                        </button>
                    </h2>
                    <div id="flush-two" class="accordion-collapse collapse" aria-labelledby="flush-two">
                        <div class="accordion-body">
                            <code>
                                <div class="geo">
                                    <p>
                                        <?php

                                               $spec = strtoupper($fspec);
                                                echo "FSPEC        : $spec";
                                                $_SESSION['spec'] = $spec;

                                                ?>
                                    </p>
                                    <p>
                                        <?php
                                                $spec1 = decbin(hexdec($spec));
                                                echo "FSPEC BINNER :$spec1";
                                                $_SESSION['spec1'] = $spec1;
                                                ?>
                                    </p>
                                </div>
                            </code>
                        </div>
                    </div>
                </div>
                <div class="accordion-item">
                    <h2 class="accordion-header" id="flush-headingFour">
                        <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                            data-bs-target="#flush-one" aria-expanded="false" aria-controls="flush-one">
                            <strong>ASTERIX Message</strong>

                        </button>
                    </h2>
                    <div id="flush-one" class="accordion-collapse collapse collapse show" aria-labelledby="flush-one">
                        <div class="accordion-body">
                            <code>
                                <div class="accordion accordion-flush" id="accordionFlushExample">
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="flush-headingOne">
                                            <button style="color:gray" class="accordion-button collapsed" type="button"
                                                data-bs-toggle="collapse" data-bs-target="#flush-collapseOne"
                                                aria-expanded="false" aria-controls="flush-collapseOne">
                                                <b> Data Source Identification</b>
                                            </button>
                                        </h2>
                                        <div id="flush-collapseOne" class="accordion-collapse collapse show"
                                            aria-labelledby="flush-headingOne">
                                            <div class="accordion-body">
                                                <code>
                                                    <?php
                                                        $kota = [
                                                              '1' => 'Medan', 
                                                              '2' => 'Pekanbaru', 
                                                              '3' => 'Tanjung Pinang', 
                                                              '4' => 'Palembang', 
                                                              '5' => 'Jakarta', 
                                                              '6' => 'Pontianak', 
                                                              '7' => 'Yogyakarta', 
                                                              '8' => 'Surabaya', 
                                                              '9' => 'Makassar', 
                                                              '10' => 'Banjarmasin', 
                                                              '11' => 'Balikpapan', 
                                                              '12' => 'Denpasar', 
                                                              '13' => 'Biak', 

                                                            
                                                        ]
                                                        
                                                        ?>

                                                    <div class="box">
                                                        <div class="DSI" style="margin-bottom: 10px;">
                                                        <p>   SAC : <?php echo ($data[$keystart[0]]) ?> ->
                                                            <?php echo hexdec($data[$keystart[0]]) ?></p>
                                                            <p>SIC : <?php echo ($data[$keystart[1]]) ?> ->
                                                                <?php echo hexdec($data[$keystart[1]]) ?></p>
                                                                <p>KOTA : <?php echo $kota[hexdec($data[$keystart[1]])] ?> </p>
                                                            <!-- <p>Kota :
                                                                    <?php $nkota = hexdec($data[$keystart[1]]);
                                                                            // echo $nkota ? $kota[$nkota] : '';
                                                                            $city = $kota[$nkota];
                                                                            $sac= hexdec($data[$keystart[0]]);
                                                                            $sic= hexdec($data[$keystart[1]]);
                                                                            $_SESSION['sic'] = $sic;
                                                                            $_SESSION['sac'] = $sac;
                                                                            $_SESSION['kota'] = $city;
                                                                            ?>
                                                                </p> -->
                                                        </div>
                                                </code>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="flush-headingThree">
                                            <button style="color:gray" class="accordion-button collapsed" type="button"
                                                data-bs-toggle="collapse" data-bs-target="#flush-collapseThree"
                                                aria-expanded="false" aria-controls="flush-collapseThree">
                                                <b>Time Of Day</b>

                                            </button>
                                        </h2>
                                        <div id="flush-collapseThree" class="accordion-collapse collapse"
                                            aria-labelledby="flush-headingThree">
                                            <div class="accordion-body">
                                                <code>
                                                    <div class=" TOD">

                                                        <?php echo $data[$keystart[2]] ?>
                                                        <?php echo $data[$keystart[3]] ?>
                                                        <?php echo $data[$keystart[4]] ?>

                                                        <p style="text-transform: uppercase;">
                                                            TOD ->
                                                            <?php $tod = hexdec($data[$keystart[2]] . $data[$keystart[3]] . $data[$keystart[4]]);
                                                                    $tod1 = $tod / 128;
                                                                    $tod2 = $tod1 / 3600;
                                                                    $tod2explode = explode(".", $tod2);
                                                                    $tod2explode1 = 0 . "." . $tod2explode[1];
                                                                    $jam = $tod2explode[0];
                                                                    $tod3 = $tod2explode1 * 60;
                                                                    $tod3explode =  explode(".", $tod3);
                                                                    $menit = $tod3explode[0];
                                                                    $tod3explode1 = 0 . "." . $tod3explode[1];
                                                                    $detik = $tod3explode1 * 60;
                                                                    echo $jam;
                                                                    echo ":";
                                                                    echo $menit;
                                                                    echo ":";
                                                                    echo round($detik, 3);
                                                                    $waktu = $jam.":".$menit.":".round($detik, 3);
                                                                    $_SESSION['waktu'] = $waktu;
                                                                    ?>
                                                        </p>
                                                    </div>
                                                </code>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="flush-headingThree1">
                                            <button style="color:gray" class="accordion-button collapsed" type="button"
                                                data-bs-toggle="collapse" data-bs-target="#flush-collapseThree1"
                                                aria-expanded="false" aria-controls="flush-collapseThree">
                                                <b>Target Report Descriptor</b>

                                            </button>
                                        </h2>
                                        <div id="flush-collapseThree1" class="accordion-collapse collapse"
                                            aria-labelledby="flush-headingThree">
                                            <div class="accordion-body">
                                                <code>
                                                    <div class=" TRD">
                                                      <P>
                                                        <?php echo $data[$keystart[5]] ?> -> <?php echo decbin(hexdec($data[$keystart[5]])) ?> 
                                                      </P>
                                                      <?php $trd = decbin(hexdec($data[$keystart[5]])) ;
                                                            $_SESSION['trd'] = $trd;
                                                            $trd = str_split($trd);
                                                            $bit6 = $trd[0].$trd[1].$trd[2];
                                                            $bit5 = $trd[3];
                                                            $bit4 = $trd[4];
                                                            $bit3 = $trd[5];
                                                            $bit2 = $trd[6];
                                                            $bit1 = $trd[7];

                                                            
                                                            $typ = [
                                                              'No Detection',
                                                              'Single PSR Detection',
                                                              'Single SSR Detection',
                                                              'PSR + SSR Detection',
                                                              'Single ModeS All-Call',
                                                              'Single ModeS Roll-Call',
                                                              'ModeS All-Call + PSR',
                                                              'ModeS ROLL-Call + PSR',
                                                            ];

                                                            $sim = $bit5 ? 'Simulated Target Report' : 'Actual Target Report';
                                                            $rdp = $bit4 ? 'Report from RDP Chain 2' : 'Report from RDP Chain 1';
                                                            $spi = $bit3 ? 'Special Position Identification' : 'Absence of SPI';
                                                            $rab = $bit2 ? 'Report from field monitor' : 'Report from aircraft transponder';
                                                            $fx = $bit1 ? 'Extention into first extent' : 'End of data item';

                                                            $_SESSION['TYP'] = $typ[bindec($bit6)];
                                                            $_SESSION['SIM'] = $sim;
                                                            $_SESSION['RDP'] = $rdp;
                                                            $_SESSION['SPI'] = $spi;
                                                            $_SESSION['RAB'] = $rab;
                                                            $_SESSION['FX'] = $fx;

                                                      ?>
                                                        <p>TYP : <?= $bit6 ?> -> <?=  $typ[bindec($bit6)];?> </p>
                                                        <p>SIM : <?= $bit5 ?> -> <?=  $sim;?> </p>
                                                        <p>RDP : <?= $bit4 ?> -> <?=  $rdp;?> </p>
                                                        <p>SPI : <?= $bit3 ?> -> <?=  $spi;?> </p>
                                                        <p>RAB : <?= $bit2 ?> -> <?=  $rab;?> </p>
                                                        <p>FX : <?= $bit1 ?> -> <?=  $fx;?> </p>
                                                    </div>
                                                </code>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="flush-headingFour">
                                            <button style="color:gray" class="accordion-button collapsed" type="button"
                                                data-bs-toggle="collapse" data-bs-target="#flush-collapseFour"
                                                aria-expanded="false" aria-controls="flush-collapseFour">
                                                <b>Measure Position In Polar Coordinates</b>

                                            </button>
                                        </h2>
                                        <div id="flush-collapseFour" class="accordion-collapse collapse"
                                            aria-labelledby="flush-headingFour">
                                            <div class="accordion-body">
                                                <code>
                                                    <div class=" TRD">
                                                      <?php
                                                      $first = hexdec($data[$keystart[6]].$data[$keystart[7]]);
                                                      $second = hexdec($data[$keystart[8]].$data[$keystart[9]]);
                                                            $lsb = $first/256;
                                                            $lsb1 = $second * 0.0054931640625;

                                                            $_SESSION['Range'] = round($lsb,2);
                                                            $_SESSION['Azimuth'] = round($lsb1 , 2);
                                                      ?>
                                                      <p>1. <?= $data[$keystart[6]].$data[$keystart[7]] ?> => <?= $first ?></p>
                                                      <p>2. <?= $data[$keystart[8]].$data[$keystart[9]] ?> => <?= $second ?></p>
                                                      <p>Range : <?= round($lsb,2) ?>NM</p>
                                                      <p>Azimuth : <?= round($lsb1 , 2)?>&deg;</p>
                                                    </div>
                                                </code>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="flush-headingFive">
                                            <button style="color:gray" class="accordion-button collapsed" type="button"
                                                data-bs-toggle="collapse" data-bs-target="#flush-collapseFive"
                                                aria-expanded="false" aria-controls="flush-collapseFive">
                                                <b>Mode 3/A Code In Octal Representation</b>

                                            </button>
                                        </h2>
                                        <div id="flush-collapseFive" class="accordion-collapse collapse"
                                            aria-labelledby="flush-headingFive">
                                            <div class="accordion-body">
                                                <code>
                                                    <div class=" TRD">
                                                      <?php 
                                                      $mode =  $data[$keystart[10]];
                                                      $mode1 =  $data[$keystart[11]];

                                                      $modeshow = sprintf("%08d", decbin(hexdec($mode)));
                                                      $modeshow1 = sprintf("%08d", decbin(hexdec($mode1)));
                                                      

                                                     $modesplit = str_split($modeshow.$modeshow1);

                                                     $v = ["Code Validated", "Code not validated"];
                                                     $g = ['Default', 'Garbled code'];
                                                     $l = ['Mode-3/A code derived from the reply of the transponder', 
                                                            'Mode-3/A code not extracted during last scan'
                                                        ];

                                                      
                                                    

                                                    $a4 = $modesplit[4] ? 4 : 0;
                                                    $a2 = $modesplit[5] ? 2 : 0;
                                                    $a1 = $modesplit[6] ? 1 : 0;
                                                    $b4 = $modesplit[7] ? 4 : 0;
                                                    $b2 = $modesplit[8] ? 2 : 0;
                                                    $b1 = $modesplit[9] ? 1 : 0;
                                                    $c4 = $modesplit[10] ? 4 : 0;
                                                    $c2 = $modesplit[11] ? 2 : 0;
                                                    $c1 = $modesplit[12] ? 1 : 0;
                                                    $d4 = $modesplit[13] ? 4 : 0;
                                                    $d2 = $modesplit[14] ? 2 : 0;
                                                    $d1 = $modesplit[15] ? 1 : 0;
                                                    
                                                    $a = $a4+$a2+$a1;
                                                    $b = $b4+$b2+$b1;
                                                    $c = $c4+$c2+$c1;
                                                    $d = $d4+$d2+$d1;


                                                    $_SESSION['v'] = $v[$modesplit[0]] ;
                                                    $_SESSION['g'] = $g[$modesplit[1]] ;
                                                    $_SESSION['l'] = $l[$modesplit[2]] ;

                                                      ?>
                                                      <P>
                                                        <?= $mode.$mode1 ?> -> <?= $modeshow.$modeshow1 ?>
                                                      </P>

                                                      <p>V : <?= $modesplit[0] ." -> ". $v[$modesplit[0]]?></p>
                                                      <p>G : <?= $modesplit[1] ." -> ". $g[$modesplit[1]]?></p>
                                                      <p>L : <?= $modesplit[2] ." -> ". $l[$modesplit[2]]?></p>
                                                      <p>
                                                        <ul>
                                                            <li>A4A2A1 : <?= $a4 . '+' . $a2 . '+' . $a1. " = " .$a ?></li>
                                                            <li>B4B2B1 : <?= $b4. '+' . $b2 . '+' . $b1 . " = " .$b ?></li>
                                                            <li>C4C2C1 : <?= $c4. '+' . $c2 . '+' . $c1 . " = " .$c ?></li>
                                                            <li>D4D2D1 : <?= $d4. '+' . $d2 . '+' . $d1 . " = " .$d ?></li>
                                                        </ul>
                                                      </p>
                                                      <p>
                                                        Mode 3/A code : <?= $a.$b.$c.$d ?>
                                                      </p>

                                                      <?php  $_SESSION['Mode'] = $a.$b.$c.$d ; ?>
                                                    </div>
                                                </code>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="flush-headingSix">
                                            <button style="color:gray" class="accordion-button collapsed" type="button"
                                                data-bs-toggle="collapse" data-bs-target="#flush-collapseSix"
                                                aria-expanded="false" aria-controls="flush-collapseSix">
                                                <b>Flight Level In Binary Representation</b>

                                            </button>
                                        </h2>
                                        <div id="flush-collapseSix" class="accordion-collapse collapse"
                                            aria-labelledby="flush-headingSix">
                                            <div class="accordion-body">
                                                <code>
                                                    <div class=" TRD">
                                                     
                                                            <?php 
                                                            $fl =  $data[$keystart[12]];
                                                            $fl1 =  $data[$keystart[13]];

                                                            $flshow = sprintf("%08d", decbin(hexdec($fl)));
                                                            $flshow1 = sprintf("%08d", decbin(hexdec($fl1)));
                                                            $flshowsplit = str_split($flshow.$flshow1);

                                                            $v = ['Code validated', 'Code not validated'];
                                                            $g = ['Default', 'Garbled code']
                                                            ?>

                                                            <p> <?= $fl.$fl1 ?> = <?= $flshow.$flshow1 ?> </p>
                                                            <p> V : <?= $flshowsplit[0] ?> -> <?= $v[$flshowsplit[0]] ?> </p>
                                                            <p> G : <?= $flshowsplit[1] ?> -> <?= $v[$flshowsplit[1]] ?> </p>
                                                            <p> FL : <?= hexdec($fl.$fl1) ?> -> <?= hexdec($fl.$fl1)/4 ?> </p>

                                                            <?php
                                                                $_SESSION['V'] = $v[$flshowsplit[0]];
                                                                $_SESSION['G'] = $v[$flshowsplit[1]];
                                                                $_SESSION['FL'] = hexdec($fl.$fl1)/4;
                                                            ?>
                                                            


                                                    </div>
                                                </code>
                                            </div>
                                        </div>
                                    </div>
                            </code>
                        </div>
                    </div>
                </div>

        <?php  }else{; ?>
        <div class="accordion-item">
            <h2 class="accordion-header" id="flush-headingSix">
                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                    data-bs-target="#flush-collapseSix" aria-expanded="false" aria-controls="flush-collapseSix">
                    Data tidak sesuai
                </button>
            </h2>
            <div id="flush-collapseSix" class="accordion-collapse collapse" aria-labelledby="flush-headingSix">
                <div class="accordion-body">
                    <code>
                        <div class="geo">
                            <p>
                                Silahkan memasukkan data asterix category 34 atau 48
                            </p>
                        </div>
                    </code>
                </div>
            </div>
        </div>
        <?php  }; ?>
        <?php  }; ?>

        </div>

        </div>
    </header>

    <!-- Footer-->
    <footer class="footer py-4">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-4 text-lg-start">Copyright &copy; Your Website 2021</div>
                <div class="col-lg-4 my-3 my-lg-0">
                    <a class="btn btn-dark btn-social mx-2" href="#!">
                        <i class="fab fa-twitter"></i>
                    </a>
                    <a class="btn btn-dark btn-social mx-2" href="#!">
                        <i class="fab fa-facebook-f"></i>
                    </a>
                    <a class="btn btn-dark btn-social mx-2" href="#!">
                        <i class="fab fa-linkedin-in"></i>
                    </a>
                </div>
                <div class="col-lg-4 text-lg-end">
                    <a class="link-dark text-decoration-none me-3" href="#!">Privacy Policy</a>
                    <a class="link-dark text-decoration-none" href="#!">Terms of Use</a>
                </div>
            </div>
        </div>
    </footer>
    <!-- Portfolio Modals-->
    <!-- Bootstrap core JS-->
    <script src="js/js.js"></script>
    <!-- Core theme JS-->
    <script src="js/scripts.js"></script>
    <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
    <!-- * *                               SB Forms JS                               * *-->
    <!-- * * Activate your form at https://startbootstrap.com/solution/contact-forms * *-->
    <!-- * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *-->
    <script src="https://cdn.startbootstrap.com/sb-forms-latest.js"></script>

    <script>
        function viewText(name) {

            document.getElementById("cnth").innerHTML = name;
        }
    </script>
    <script>
        var loader = document.getElementById('preload');


        //Hide the spinner after 2 seconds
        setTimeout(
            function () {
                var id200 = document.getElementById("preload");
                id200.style.transition = "opacity " + 1 + "s";
                id200.style.opacity = 0;
                id200.addEventListener("transitionend", function () {
                    console.log("transition has ended, set display: none;");
                    id200.style.display = "none";
                });
            }, 1000
        );
    </script>
</body>

</html>