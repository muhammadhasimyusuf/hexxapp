<?php
session_start();
require 'vendor/autoload.php';
// reference the Dompdf namespace
use Dompdf\Dompdf;

$html = $_SESSION['dec'] == 2 ? '

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

    <p><b>DATA:</b> <br>'.$_SESSION['hexa'].'</p>
    <p>1. <b>Category </b> :'.$_SESSION['cat'].'</p>
    <p>2. <b>Length </b>:'.$_SESSION['len'].'</p>
    
    <p>3.<b> FSPEC </b>: 
        <ul>
            <li>'.$_SESSION['spec'].'</li>
            <li>'.$_SESSION['spec1'].'</li>
        </ul>
    </p>
    <p>4.<b> ASTERIX Message </b>:</p>
    <p>- Data Source Identification :
        <ul>
            <li>SAC:'.$_SESSION['sac'].'</li>
            <li>SIC:'.$_SESSION['sic'].'</li>
            <li>Kota'.$_SESSION['kota'].'</li>
        </ul>
    </p>
    <p>
        - Message Type :
        <ul>
            <li>'.$_SESSION['dec'].': <span>'.$_SESSION['decData'].'</span> </li>
        </ul>
    </p>
    <p>
        - Time Of Day :
        <ul>
            <li>'.$_SESSION['tod'].': <span>'.$_SESSION['waktu'].'</span> </li>
        </ul>
    </p>

        <p>
        - Sector Number:
        <ul>
            <li>SN :'.$_SESSION['tod'].'-> <span>'.$_SESSION['secresult'].'</span> </li>
        </ul>
    </p>
    ;


</body>
</html>

'
:
'

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>

    <p><b>DATA:</b> <br>'.$_SESSION['hexa'].'</p>
    <p>1. <b>Category </b> :'.$_SESSION['cat'].'</p>
    <p>2. <b>Length </b>:'.$_SESSION['len'].'</p>
    
    <p>3.<b> FSPEC </b>: 
        <ul>
            <li>'.$_SESSION['spec'].'</li>
            <li>'.$_SESSION['spec1'].'</li>
        </ul>
    </p>
    <p>4.<b> ASTERIX Message </b>:</p>
    <p>- Data Source Identification :
        <ul>
            <li>SAC:'.$_SESSION['sac'].'</li>
            <li>SIC:'.$_SESSION['sic'].'</li>
            <li>Kota'.$_SESSION['kota'].'</li>
        </ul>
    </p>
    <p>
        - Message Type :
        <ul>
            <li>'.$_SESSION['dec'].': <span>'.$_SESSION['decData'].'</span> </li>
        </ul>
    </p>
    <p>
        - Time Of Day :
        <ul>
            <li>'.$_SESSION['tod'].': <span>'.$_SESSION['waktu'].'</span> </li>
        </ul>
    </p>



</body>
</html>

';

if($_SESSION['cat'] == 48){
    $html = '

    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
    </head>
    <body>
    
        <p><b>DATA:</b> <br>'.$_SESSION['hexa'].'</p>
        <p>1. <b>Category </b> :'.$_SESSION['cat'].'</p>
        <p>2. <b>Length </b>:'.$_SESSION['len'].'</p>
        
        <p>3.<b> FSPEC </b>: 
            <ul>
                <li>'.$_SESSION['spec'].'</li>
                <li>'.$_SESSION['spec1'].'</li>
            </ul>
        </p>
        <p>4.<b> ASTERIX Message </b>:</p>
        <p>- Data Source Identification :
            <ul>
                <li>SAC:'.$_SESSION['sac'].'</li>
                <li>SIC:'.$_SESSION['sic'].'</li>
                <li>Kota'.$_SESSION['kota'].'</li>
            </ul>
        </p>
        <p>
            - Time Of Day :
            <ul>
                <li>'.$_SESSION['tod'].': <span>'.$_SESSION['waktu'].'</span> </li>
            </ul>
        </p>
        <p>
            -Target Report Descriptor :
            <ul>
                <li>TYP :'. $_SESSION['TYP'] .' </li>
                <li>SIM :'. $_SESSION['SIM'] .' </li>
                <li>RDP :'. $_SESSION['RDP'] .' </li>
                <li>SPI :'. $_SESSION['SPI'] .' </li>
                <li>RAB :'. $_SESSION['RAB'] .' </li>
                <li>FX :'. $_SESSION['FX'] .' </li>
            </ul>
        </p>
        <p>
            -Measure Position In Polar Coordinates :
            <ul>
                <li>Range :'. $_SESSION['Range'] .' </li>
                <li>Azimuth :'. $_SESSION['Azimuth'] .' </li>
            </ul>
        </p>
        <p>
            -Mode 3/A Code In Octal Representation :
            <ul>
                <li>V :'. $_SESSION['v'] .' </li>
                <li>G :'. $_SESSION['g'] .' </li>
                <li>L :'. $_SESSION['l'] .' </li>
                <li>Mode :'. $_SESSION['Mode'] .' </li>
            </ul>
        </p>
        <p>
            -Flight Level In Binary Representation :
            <ul>
                <li>V :'. $_SESSION['V'] .' </li>
                <li>G :'. $_SESSION['G'] .' </li>
                <li>FL :'. $_SESSION['FL'] .' </li>
            </ul>
        </p>
    
    
    
    </body>
    </html>
    
    ';
}
// instantiate and use the dompdf class
$dompdf = new Dompdf();
$dompdf->loadHtml($html);

// (Optional) Setup the paper size and orientation
$dompdf->setPaper('A4', 'portait');

// Render the HTML as PDF
$dompdf->render();

// Output the generated PDF to Browser
$dompdf->stream();


?>
